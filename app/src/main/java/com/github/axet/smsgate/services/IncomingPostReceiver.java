package com.github.axet.smsgate.services;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsMessage;

import com.github.axet.smsgate.app.MainApplication;
import com.github.axet.smsgate.app.ScheduleSMS;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@TargetApi(19) // SMS_DELIVER is API19+
public class IncomingPostReceiver extends BroadcastReceiver {

    public static class Message {
        public String phone;
        public String body;

        public Message(String p, String b) {
            this.phone = p;
            this.body = b;
        }
    }

    public static HashMap<String, Message> getMessagesFromIntent(Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            return null;
        }

        Object[] pdusObj = (Object[]) bundle.get("pdus");

        SmsMessage[] messages = new SmsMessage[pdusObj.length];

        for (int i = 0; i < messages.length; i++) {
            if (Build.VERSION.SDK_INT >= 23) {
                String format = intent.getStringExtra("format");
                messages[i] = SmsMessage.createFromPdu((byte[]) pdusObj[i], format);
            } else {
                messages[i] = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
            }
        }

        HashMap<String, Message> map = new HashMap<>();
        for (SmsMessage msg : messages) {
            String id = msg.getOriginatingAddress() + " " + msg.getTimestampMillis();
            Message m = map.get(id);
            if (m != null) {
                m.body += msg.getMessageBody();
                map.put(id, m);
            } else {
                map.put(id, new Message(msg.getOriginatingAddress(), msg.getMessageBody()));
            }
        }
        return map;
    }

    public static boolean filter(List<ScheduleSMS> items, Message m) {
        for (ScheduleSMS item : items) {
            if (item.hide) {
                String bm = m.body.trim();
                String bi = item.message.trim();
                if (PhoneNumberUtils.compare(item.phone, m.phone) && bm.equals(bi))
                    return true;
            }
        }
        return false;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String a = intent.getAction();
        if (a == null)
            return;
        if (Telephony.Sms.Intents.SMS_DELIVER_ACTION.equals(a)) {
            Map<String, Message> map = getMessagesFromIntent(intent);
            if (map == null)
                return;
            List<ScheduleSMS> items = MainApplication.load(context);
            ContentResolver res = context.getContentResolver();
            for (String id : map.keySet()) {
                Message m = map.get(id);
                if (filter(items, m))
                    continue;
                ContentValues values = new ContentValues();
                values.put(Telephony.Sms.ADDRESS, m.phone);
                values.put(Telephony.Sms.BODY, m.body);
                res.insert(Telephony.Sms.Inbox.CONTENT_URI, values);
            }
        }
    }
}
