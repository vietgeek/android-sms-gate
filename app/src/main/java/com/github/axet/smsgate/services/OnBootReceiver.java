/* Copyright (c) 2009 Christoph Studer <chstuder@gmail.com>
 * Copyright (c) 2010 Jan Berkel <jan.berkel@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.axet.smsgate.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import com.github.axet.androidlibrary.services.WifiKeepService;
import com.github.axet.smsgate.app.MainApplication;
import com.github.axet.smsgate.dialogs.RebootDialogFragment;
import com.zegoggles.smssync.activity.SMSGateFragment;
import com.zegoggles.smssync.service.SmsBackupService;

import static com.zegoggles.smssync.App.LOCAL_LOGV;
import static com.zegoggles.smssync.App.TAG;

public class OnBootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (LOCAL_LOGV) Log.v(TAG, "onReceive(" + context + "," + intent + ")");
        start(context);
    }

    public static void start(Context context) {
        main(context);
        fragment(context);
    }

    public static void main(Context context) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);

        SMSGateFragment.checkPermissions(context);
        ScheduleService.start(context);
        if (Build.VERSION.SDK_INT >= 18)
            NotificationListener.startIfEnabled(context);
        NotificationService.startIfEnabled(context);
        RebootDialogFragment.schedule(context);
        WifiKeepService.startIfEnabled(context, shared.getBoolean(MainApplication.PREF_WIFIRESTART, false));
    }

    public static void fragment(Context context) {
        SMSGateFragment.checkPermissions(context);
        FirebaseService.startIfEnabled(context);
        SmsReplyService.startIfEnabled(context);
        SmsBackupService.scheduleBootupBackup(context);
    }
}
